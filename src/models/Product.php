<?php
// product parent class that holds all the properties
abstract class Product 
{
    public $type;
    public $sku;
    public $name;
    public $price;
    // default values - these properties wont allways need to be set
    public $size = NULL;
    public $weight = NULL;
    public $height = NULL;
    public $width = NULL;
    public $length = NULL;
    // assign input values to properties
    public function __construct($type, $sku, $name, $price)
    {
        $this->type = $type;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }
    // method to make an array from product values
    public function makeArr()
    {
    return array ("type"=>$this->type, "sku"=>$this->sku, "name"=>$this->name, "price"=>$this->price,
     "size"=>$this->size, "weight"=>$this->weight, "height"=>$this->height, "width"=>$this->width, "length"=>$this->length);
    }
}
