<?php
// this child class takes in form data for DVDs and processes it further
class AddDvd extends Product 
{
    private $data;
    private $add;
    // run a method when instantiating the class and take input data
    public function __construct($type, $sku, $name, $price, $size)
    {
        // use parent class method to assign input values
        parent::__construct($type, $sku, $name, $price);
        // assign input value that's specific to this product to a parent class property
        $this->size = $size;
        // use method from parent class and create an array from product values
        $this->data = $this->makeArr();
        // instantiate a class to further process data to database
        $add = new AddItem($this->data);
    }
}
