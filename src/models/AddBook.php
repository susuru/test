<?php
// this child class takes in form data for Books and processes it further
class AddBook extends Product 
{
    private $data;
    private $add;
    // run a method when instantiating the class and take input data
    public function __construct($type, $sku, $name, $price, $weight)
    {
        // use parent class method to assign input values
        parent::__construct($type, $sku, $name, $price);
        // assign input value tht's specific to this product to a parent class property
        $this->weight = $weight;
        // use method from parent class and create an array from product values
        $this->data = $this->makeArr();
        // instantiate a class to further process data to database
        $add = new AddItem($this->data);
    }
}

