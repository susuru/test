<?php
// this class outputs html warning message 
class WarningMessage
{
    private $sku;
    // run method automatically as class is instantiated
    public function __construct($sku)
    {
        // assign input value to property
        $this->sku = $sku;
        // output the message text
        echo "<p class=". "text-danger" .">Item with SKU - ". $this->sku ."
        already exists. Please add other product.</p>";
    }
}
