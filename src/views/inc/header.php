<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- some custom css -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Scandiweb Test</title>
</head>
<body>
    <!-- load the navbar -->
    <?php require_once("../src/views/inc/navbar.php");?>
    <!-- loading classes -->
    <?php require_once("../src/helpers/NavBar.php");?>
    <?php require_once("../src/helpers/Database.php");?>
    <?php require_once("../src/controller/GetPost.php");?>
    <?php require_once("../src/controller/MassDelete.php");?>    
    <?php require_once("../src/helpers/TypeCaseFactory.php");?>
    <?php require_once("../src/models/Product.php");?>
    <?php require_once("../src/models/AddDvd.php");?>
    <?php require_once("../src/models/AddBook.php");?>
    <?php require_once("../src/models/AddFurniture.php");?>
    <?php require_once("../src/controller/AddItem.php");?>
    <?php require_once("../src/view/WarningMessage.php");?>


    <!-- wrap all page content -->
    <div class="container">
        <!-- handle navigation with a class -->
        <?php $navigation = new NavBar; ?>