<?php 
// class to add data from products to database
class AddItem
{
    private $db;
    private $data;
    private $message;
    //  method with a query to check isn't in the database already such a sku value
    public function findRowBySku($sku)
    {
        // run a query to select a particular sku value in whole database table
        $this->db->query("SELECT * FROM products WHERE sku = :sku");
        $this->db->bind(":sku", $sku);
        // run a method to get single record
        $row=$this->db->single();
        // Check if there is a record now and return a value
        if ($this->db->rowCount()>0) {
            return true;
        } else {
            return false;
        }
    }
    // run a method when instantiating the class
    public function __construct($data)
    {
        $this->data = $data;
        // create an instance from class that connects to the database and handles other methods
        $this->db = new Database;
        // check if the sku is unique - run a method defined above
        if ($this->findRowBySku($this->data["sku"]) == false) {
            // run a query for the database to insert a new row
            $this->db->query("INSERT INTO products (sku, name, price, size, weight, height, width, length)
            VALUES(:sku, :name, :price, :size, :weight, :height, :width, :length)");
            // bind named parameters to values
            $this->db->bind(":sku", $this->data["sku"]);
            $this->db->bind(":name", $this->data["name"]);
            $this->db->bind(":price", $this->data["price"]);
            $this->db->bind(":size", $this->data["size"]);
            $this->db->bind(":weight", $this->data["weight"]);
            $this->db->bind(":height", $this->data["height"]);
            $this->db->bind(":width", $this->data["width"]);
            $this->db->bind(":length", $this->data["length"]);
                
            $this->db->execute();
            // after adding product to database, redirect to product lists
            header('Location: http://localhost/mans_scandiweb/public');
        } else { 
            // if sku is not unique, then instantiate a class to output a warning message
            $this->message = new WarningMessage($this->data["sku"]);
        }
    }
}
