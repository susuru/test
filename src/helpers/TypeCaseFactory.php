<?php
//  create objects from classes according of the type from the form
class TypeCaseFactory
{
    // run a method when instantiating the class
    public function __construct(){
        // first check is the form submitted 
        if (isset($_POST["submit"])) {
            // according to the selected type create a specific object with certain properties
            switch ($_POST["select"]) {
                case "Size":
                   return new AddDvd($_POST["select"], $_POST["Sku"], $_POST["Name"], $_POST["Price"], $_POST["Size"]);
                case "Weight": 
                    return new addBook($_POST["select"], $_POST["Sku"], $_POST["Name"], $_POST["Price"], $_POST["Weight"]);
                case "Dimensions":
                    return new AddFurniture($_POST["select"], $_POST["Sku"], $_POST["Name"], $_POST["Price"],
                    $_POST["Height"], $_POST["Width"], $_POST["Length"]);  
            }
        }
    }
}